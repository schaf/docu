---
sidebar_position: 1
---

# Übersicht

:::danger Achtung

Die Dokumentation und die Schnittstellen befinden sich im Aufbau. Bitte beachten Sie, dass es zu Breaking Changes kommen kann.

:::

Hier werden Schnittstellen und Prozesse beschrieben, um Produkte im Ärzteverlag-Shop zu erstellen, zu aktualisieren und zu löschen.


## Externe Produkte (z. B. Semiro)

Der Kaufabschluss wird separat behandelt und [hier](/docs/purchase/purchase#semiro-produkt) beschrieben.


