---
sidebar_position: 1
---

# Übersicht
:::danger Achtung

Die Dokumentation und die Schnittstellen befinden sich im Aufbau. Bitte beachten Sie, dass es zu Breaking Changes kommen kann.

:::

## API Key
Um die Kommunikation mit der REST API zu ermöglichen, benötigt man zuerst einen API KEY. Die Erstellung des API KEYs
wird [hier](https://woocommerce.com/document/woocommerce-rest-api/#section-2) beschrieben.

## Testumgebung

Je nachdem auf welcher Umgebung man sich befindet, ändern sich URLs und Features. Es gibt eine `int` Umgebung, 
die den jeweiligen Enwicklungsstatus wiedergibt. Eine `uat` Umgebung auf der die neusten Features auf Stabilität hin
getestet werden und die Produktivumgebung `prod`.

* `int` - `https://shop.int-tutool.io/` - Development
* `uat` - `https://shop.uat-tutool.io/` - Akzeptanztest
* `prod` - `https://shop.tutool.io/` - Produktivumgebung