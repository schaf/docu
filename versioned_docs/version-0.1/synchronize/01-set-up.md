---
sidebar_position: 1
---

# Übersicht
:::danger Achtung

Die Dokumentation und die Schnittstellen befinden sich im Aufbau. Bitte beachten Sie, dass es zu Breaking Changes kommen kann.

:::

### **Der gesamte Abschnitt gilt nur für Tutoolio Produkte**  
Damit Kurse aus dem Tutoolio LMS heraus synchronisiert werden können, benötigt man einen API Key, um die Rechte zu 
erhalten, Kurse von einem Management Level im Shop zu synchronisieren und sie als Produkte erstellen sowie verkaufen 
zu können. Beim Verkauf eines Kurses im Shop wird der Kurs automatisch im Tutoolio LMS an den Käufer augeliefert.


## API Key
Je nachdem, aus welchem Mandanten oder aus welcher Instanz die Kurse als Basis dienen sollen, um im Shop als
Produkt erstellt zu werden, muss auf dieser Ebene ein API Key erstellt werden. Diesen API Key kann man in den 
jeweiligen Einstellungen erstellen und anzeigen lassen.

Nachdem man sich diesen Key erstellt hat, muss man diesen im jeweiligen Shop einsetzen. Hierzu öffnet man die `Tutoolio LMS Settings`
und den Reiter `Settings` und setzt dort im Feld Secret-Key den vorgesehenen Schlüssel ein.

![Api Key](/img/synchronize-api-key.png)


## Manuelle Synchronisation einrichten

In den Einstellungen `Tutoolio LMS Settings` im Reiter `Synchronization` ist man in der Lage, die Kurse manuell zu synchronisieren. 
Dazu betätigt man den Button `Load courses`, um die Kurse aus dem Tutoolio LMS zu laden. Im nächsten Schritt folgt dann das Erstellen der Kurse,
entweder einzeln oder alle.

![Wordpress Settings](/img/synchronize.png)

## Automatisierte Synchronisation

`WIP`

