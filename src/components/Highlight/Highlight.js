import React from "react";

export const Highlight = ({children, color, inner}) => (
    <span
        style={{
            display: inner ? 'inline':'block',
            width: '100%',
            backgroundColor: color,
            borderRadius: '4px',
            color: '#fff',
            padding: inner ? '5px':'10px',
            cursor: 'pointer',
            fontSize: 'small',
        }}>
    {children}
  </span>
);