import React from 'react';

export const Label = ({children}) => (
    <i
        style={{
            backgroundColor: '#efefef',
            color: '#999',
            padding: '2px 4px',
            borderRadius: '2px',
            fontStyle: 'normal',
        }}>
        {children}
    </i>
);