import React from 'react';

export const InfoLabel = ({children}) => (
    <i
        style={{
            backgroundColor: '#efefef',
            color: '#999',
            float: 'right',
            fontWeight: 700,
            fontSize: '70%',
            padding: '2px 4px',
            marginLeft: '5px',
            borderRadius: '2px',
            textTransform: 'uppercase',
            fontStyle: 'normal',
            whiteSpace: 'nowrap',
        }}>
        {children}
    </i>
);