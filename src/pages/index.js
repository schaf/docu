import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import styles from './index.module.css';
import HomepageFeatures from '@site/src/components/HomepageFeatures';

function HomepageHeader() {
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">
          <p className={styles.heroSubtitle}>Deutscher Ärzteverlag</p>
        <h1 className="hero__title">Dokumentation</h1>

        <div className={styles.buttons}>
          <Link
            className="button button--secondary button--lg"
            to="/docs/intro">
            Start
          </Link>
        </div>
      </div>
    </header>
  );
}

export default function Home() {
  return (
    <Layout
      title='Tutoolio Documentation'
      description="Tutoolio Documentation">
      <HomepageHeader />

    </Layout>
  );
}
